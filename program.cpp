/*
Creator: Alexander Øvergård
Name of machine: THE JUTEBOX-TETRIFY_5000!

tetris.h is not self made, but found from website found in file!
only small modification to fit this project done.
*/


#include <LiquidCrystal_I2C.h>
#include "tetris.h"

//LCD
LiquidCrystal_I2C lcd(0x27,20,4);

//servos
#define milk 2
#define coffee 1
#define tea 0

//Buzzer
#define buzzerPin 8

//joystick
#define joySelect 3
#define vert_joy A1
#define horz_joy A0
bool button_debounce = false;
bool joy_debounce = false;
int pos = 0;

//leds
#define yellow 12
#define blue 11
#define red 10

int led = 0;

void setup() {
  // stepper motor setup:
  pinMode(milk, OUTPUT);
  pinMode(coffee, OUTPUT);
  pinMode(tea, OUTPUT);
  stepMotor(milk, 102);
  stepMotor(coffee, 102);
  stepMotor(tea, 101);

  // joystick setup:
  pinMode(joySelect, INPUT_PULLUP);
  pinMode(vert_joy, INPUT);
  pinMode(horz_joy, INPUT);

  //LCD setup:
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Bewerages:");
  lcd.setCursor(1,1);
  lcd.print("Milk");
  lcd.setCursor(1,2);
  lcd.print("Coffee");
  lcd.setCursor(1,3);
  lcd.print("Tea");

  //buzzer setup:
  pinMode(buzzerPin, OUTPUT);

  // leds
  pinMode(yellow, OUTPUT);
  pinMode(blue, OUTPUT);
  pinMode(red, OUTPUT);
}

void loop() {  
  for (int thisNote = 0; thisNote < notes * 2; thisNote += + 2) {
    lights(led);
    
    playTetris(thisNote);
    
    int vert = map(analogRead(vert_joy), 0, 1023, -100, 100);

    updatePos(vert, pos);
    updateScreen(pos);

    if(digitalRead(joySelect) == LOW && button_debounce == false){
      button_debounce = true;
      giveDrink(pos);
    }
    if(digitalRead(joySelect) == HIGH && button_debounce == true){
      button_debounce = false;
    }
  }
}

void lights(int &val){
  if(val < 2){
    val += 1;
  }
  else{
    val = 0;
  }

  switch(val){
    case 0:
      digitalWrite(yellow, HIGH);
      digitalWrite(blue, LOW);
      digitalWrite(red, LOW);
      break;
    case 1:
      digitalWrite(yellow, LOW);
      digitalWrite(blue, HIGH);
      digitalWrite(red, LOW);
      break;
    case 2:
      digitalWrite(yellow, LOW);
      digitalWrite(blue, LOW);
      digitalWrite(red, HIGH);
      break;
    default:
      digitalWrite(yellow, HIGH);
      digitalWrite(blue, HIGH);
      digitalWrite(red, HIGH);
      break;
  }
}

void giveDrink(int &pos){
  switch(pos){
    case 0:
      stepMotor(milk, 200);
      break;
    case 1:
      stepMotor(coffee, 200);
      break;
    case 2:
      stepMotor(tea, 200);
      break;
    default:
      break;
  }
}

void stepMotor(int motor, int steps){
  for(int i = 0; i < steps; i++){
    digitalWrite(motor, HIGH);
    delay(10);
    digitalWrite(motor, LOW);
  }
}

void startUpSong(){
}

void updatePos(int &joy_position, int &pos){
  if(joy_position == 100 && joy_debounce == false){
    if(pos > 0){
      pos-= 1;
    }
    else {
      pos = 2;
    }
    joy_debounce = true;
  }
  if(joy_position == -100 && joy_debounce == false){
    if(pos < 2){
      pos += 1;
    }
    else{
      pos = 0;
    }
    joy_debounce = true;
  }
  if(joy_position == 0 && joy_debounce == true){
    joy_debounce = false;
  }
}

void updateScreen(int pos){
  switch(pos){
    case 0:
      lcd.setCursor(0, 1);
      lcd.print("*");
      lcd.setCursor(0, 2);
      lcd.print(" ");
      lcd.setCursor(0, 3);
      lcd.print(" ");
      break;
    case 1:
      lcd.setCursor(0, 1);
      lcd.print(" ");
      lcd.setCursor(0, 2);
      lcd.print("*");
      lcd.setCursor(0, 3);
      lcd.print(" ");
      break;
    case 2:
      lcd.setCursor(0, 1);
      lcd.print(" ");
      lcd.setCursor(0, 2);
      lcd.print(" ");
      lcd.setCursor(0, 3);
      lcd.print("*");
      break;
    default:
      lcd.setCursor(0, 1);
      lcd.print(" ");
      lcd.setCursor(0, 2);
      lcd.print(" ");
      lcd.setCursor(0, 3);
      lcd.print(" ");
      break;
  }
}
